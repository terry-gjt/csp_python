# 非零段划分
# 思路:
# 先遍历一遍，找到所有的突出，记录左右侧最低点中的较高值和最高点，p在这个两者范围内则突出是一个非零端
# 对所有的突出排序，若突起上界的最小值大于下界的最大值，则所有突起都能满足，否则在这两个值之间取p，遍历所有突起

def takefirst(elem):
    return elem[0]

def st20210902():
    m=int(input())
    numlist = list(map(int, input().split()))
    prominent = []
    tempmax=0
    leftmin=10000
    rightmin = 10000
    for i in range(len(numlist)):
        if


    yAndResult={}
    for i in range(m):
        listb=list(map(int,input().split()))
        num=listb[0]
        temp=yAndResult.get(num, [0,0])#存入0和1的个数
        if listb[1]==0:
            temp[0]+=1
        else:
            temp[1]+=1
        yAndResult[num]=temp
    # yAndResult = {5: [2, 1], 2: [0, 1], 3: [1, 0], 4: [1, 0], 1000000: [0, 1], 1: [1, 0]}
    # yAndResult = {1: [1, 0], 2: [0, 1], 3: [1, 0], 4: [1, 0], 5: [2, 1] ,1000000: [0, 1]}
    yAndResultKeys=list(yAndResult.keys())
    yAndResultKeys.sort()
    beforeErrorNum = []
    behindErrorNum = []
    ErrorNum = []
    temp=0
    for i in yAndResultKeys:
        temp+=yAndResult[i][1]
        beforeErrorNum.append(temp)
    temp2 = 0
    for i in range(len(yAndResultKeys)-1,-1,-1):
        temp2+=yAndResult[yAndResultKeys[i]][0]
        behindErrorNum.append(temp2)
    behindErrorNum.reverse()
    # print(beforeErrorNum)
    # print(behindErrorNum)
    for i in range(len(beforeErrorNum)):
        ErrorNum.append(beforeErrorNum[i]+behindErrorNum[i])
    # print(ErrorNum)
    temp=0
    for i in range(1,len(ErrorNum)):
        if ErrorNum[temp]>=ErrorNum[i]:
            temp=i
    print(yAndResultKeys[temp])
st20210902()